var fs = require('fs');
require('shelljs/global');
!reg
var fileName = process.argv[2];
var message = fs.readFileSync(fileName, "utf8").trim();

var regEx = new RegExp(/^\[.+\]/);

if (!regEx.test(message)) {
    console.log("Error Commit Message not in proper format");
    console.log("  ");
    console.log("  Example of valid mesage:");
    console.log("  [Dev Name] Fixed the ui styling for button");
    process.exit(1);
}
